# CYCLE is a Yummy Card Layout Engine

> Licence libre, laquelle ?
>
> * [Logiciel libre](https://fr.wikipedia.org/wiki/Logiciel_libre#Domaine_public)
> * [GNU GPL](https://fr.wikipedia.org/wiki/Licence_publique_g%C3%A9n%C3%A9rale_GNU)

## Attribution

Ce projet s'inspire des travaux sur [nanDECK](http://www.nand.it/nandeck/index.html).

<img src="./images/CYCLE.png" width="300px" />

## Description

CYCLE aspire à devenir un outil de maquetage de cartes à jouer et de génération de decks.

## Features

* Ajout d'image/texte à un canvas, position de l'origne x/y + dimensionement
* Gestion des médias : import images (png/svg), fonts (local/folder ?)
* Export png/pdf ( et pages complète)
* Scripting (input) et tableur intégrer / Import ods/xls et script
* Génération de deck (script )

***

- Gestion de calque ?
- Création template de cartes (et deck ?)
- BDD ? Boarf.

## Techs

* Web ?
* Raphael
* Pas d'angular, from scratch
* Utiliser les propriété CSS

***

* [Canvas](https://developer.mozilla.org/fr/docs/Tutoriel_canvas) sur la MDN et la [Savegarde des images](https://developer.mozilla.org/fr/docs/Tutoriel_canvas/Pixel_manipulation_with_canvas#Sauvegarde_des_images)
* [SheetJS](https://sheetjs.com/) et le [GitHub](https://github.com/SheetJS/js-xlsx)
* [Canvas](https://www.w3schools.com/graphics/canvas_intro.asp) sur la W3Schools