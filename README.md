# CYCLE is a Yummy Card Layout Engine

## Attribution

Ce projet s'inspire des travaux sur [nanDECK](http://www.nand.it/nandeck/index.html).

<img src="resources/images/CYCLE.png" width="300px" />

## Description

CYCLE aspire à devenir un outil de maquettage de cartes à jouer et de génération de decks.